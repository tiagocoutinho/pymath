import numpy


def path_3d_iter(x, y, z):
    #nb_points =len(x)*len(y)*len(z)
    #path = numpy.empty((nb_points, 3))
    #point_nb = 0
    xord, yord = True, True
    for i, zi in enumerate(z):
        for j, yi in enumerate(y[::1 if yord else -1]):
            for k, xi in enumerate(x[::1 if xord else -1]):
                #path[point_nb] = xi, yi, zi
                #point_nb += 1
                yield (xi, yi, zi)
            xord = not xord
        yord = not yord
    #return path


def path_3d(x, y, z):
    nb_points =len(x)*len(y)*len(z)
    path = numpy.empty((nb_points, 3))
    for i, p in enumerate(path_3d_iter(x, y, z)):
        path[i] = p
    return path


def path_3d_2(x, y, z):
    nb_points =len(x)*len(y)*len(z)
    path = numpy.empty((nb_points, 3))
    point_nb = 0
    xord, yord = True, True
    for i, zi in enumerate(z):
        for j, yi in enumerate(y[::1 if yord else -1]):
            for k, xi in enumerate(x[::1 if xord else -1]):
                path[point_nb] = xi, yi, zi
                point_nb += 1
            xord = not xord
        yord = not yord
    return path


def plot_path_3d(path):
    from matplotlib.pyplot import figure
    from mpl_toolkits.mplot3d import Axes3D
    fig = figure()
    ax = fig.gca(projection='3d')
    xx, yy, zz = path.transpose()
    ax.plot(xx, yy, zz)
    fig.show()
    
def main():
    x = numpy.linspace(4, 1, 4)
    y = numpy.linspace(1, 5, 5)
    z = numpy.linspace(-3, 0, 4)

    path = path_3d(x, y, z)
    plot_path_3d(path)

if __name__ == '__main__':
    main()




